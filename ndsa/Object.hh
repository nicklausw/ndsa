namespace NDSA {
  // objects have directions to move
  // but this will probably be moved to erin engine.
  enum Directions {
    Up, Down,
    Left, Right,
    UpLeft, UpRight,
    DownLeft, DownRight
  };
  
  class Object {
    // identifier for sprite.
    int ID;
    
    // identifier for dynamic object list.
    std::list<Object*>::iterator ListID;
    
    // and the sprite itself!
    Sprite *ObjSprite;
    
    // DUAL SCREEN SUPPORT!
    NDSA_Screen SprScreen;
    
    // all objects have code.
    void (*ObjCode)(Object *Me);
    
    // not all objects are sprites.
    bool Sprited = false;
    
    void Update() {
      // Sprite.hh is unaware of the internals
      // of the object class, so we pass manually.
      Sprites.ObjUpdate(ID, X, Y, ObjSprite, SprScreen); 
    }
    
    void AddToList() {
      Lists.Objects.push_back(this);
      ++Lists.ObjectsIt;
      ListID = Lists.ObjectsIt;
    }
    
    public:
    int X, Y;
    
    // use this for a "create" event! kinda!
    bool Initialized = false;
    
    // object movement magic
    int MovementStep = 0;
    Directions MovementDir;
    
    void Run() { ObjCode(this); }
    
    Object(void nCode(Object *Me), Sprite *nSprite, int nX, int nY, NDSA_Screen nSprScreen)
     : ObjCode(nCode), X(nX), Y(nY), ObjSprite(nSprite), SprScreen(nSprScreen) {
      ID = Sprites.Find_Slot();
      
      AddToList();
      
      
      Sprited = true;
      Update();
      Run();
    }
    
    Object(void nCode(Object *Me)) : ObjCode(nCode) { 
      // simple!
      AddToList();
      Run();
    }
    
    ~Object() {
      if (Sprited == true) {
        Sprites.Empty_Slot(ID);
        oamClear(&oamMain, ID, 1);
      }
      Lists.Objects.erase(ListID);
    }
    
    void Move(Directions Dir, int Number) {
      sassert(Sprited == true, "Movement is for sprites only");
      
      switch (Dir) {
        case Up: Y -= Number; break;
        case Down: Y += Number; break;
        case Left: X -= Number; break;
        case Right: X += Number; break;
        case UpLeft: X -= Number; Y -= Number; break;
        case UpRight: X += Number; Y -= Number; break;
        case DownLeft: X -= Number; Y += Number; break;
        case DownRight: X += Number; Y += Number; break;
      }
      
      Update();
    }
  };
}